<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Distrito extends Model
{
    protected $table = "distrito";
    public $timestamps = false; // OBVIA 2 CAMPOS CREATE_AT UPDATE_AT
}

