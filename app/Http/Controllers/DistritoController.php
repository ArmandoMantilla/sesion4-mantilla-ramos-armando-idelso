<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Distrito;
use Session;
use Illuminate\Support\Facades\Redirect;
class DistritoController extends Controller
{
    
    public function index()
    {
        $registros=Distrito::get();
        //return $registros;
        return view("distrito.listado",compact('registros'));
    }

    
    public function create()
    {
        return view("distrito.nuevo");
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'txt_nombre' => 'required',
            'txt_pais' => 'required',
            'txt_departamento' =>'required',
        ]);
        //return $request;
        $distrito=new Distrito;
        $distrito->dis_nombre=$request->txt_nombre;  
        $distrito->dis_pais=$request->txt_pais;
        $distrito->dis_departamento=$request->txt_departamento;
        $distrito->dis_fechacreacion=$request->txt_fundacion;
        $distrito->dis_cantidadpoblacion=$request->txt_poblacion;
        $distrito->dis_ubicacion=$request->txt_ubicacion;

       if($distrito->save()){
           Session::flash('exito','Registrado de manera Correcta');
           return Redirect::to('distrito');
       }
        else{
            Session::flash('error','Ocurrio un error,Verifique!');
            return Redirect::to('distrito/create');
        }


    }

   
    public function show($id)
    {
        $distrito=Distrito::findOrFail($id);
        return view('distrito.eliminar',compact('distrito'));
    }

    public function edit($id)
    {
        $distrito=Distrito::findOrFail($id);
        return view('distrito.editar',compact('distrito'));
    }

    
    public function update(Request $request, $id)
    {
        $distrito=Distrito::FindOrFail($id);
        $distrito->dis_nombre=$request->txt_nombre;  
        $distrito->dis_pais=$request->txt_pais;
        $distrito->dis_departamento=$request->txt_departamento;
        $distrito->dis_fechacreacion=$request->txt_fundacion;
        $distrito->dis_cantidadpoblacion=$request->txt_poblacion;
        $distrito->dis_ubicacion=$request->txt_ubicacion;

       if($distrito->save()){
           Session::flash('exito','Editado de manera Correcta');
           return Redirect::to('distrito');
       }
        else{
            Session::flash('error','Ocurrio un error,Verifique!');
            return Redirect::to('distrito/'.$id.'/edit');
        }
    }

    
    public function destroy($id)
    {
        try{
            //FLUJO SIN EXCEPCIONES
            Distrito::destroy($id);
            Session::flash('exito','Eliminado de manera Correcta');
            return Redirect::to('distrito');
        }catch(\Illuminate\Database\QueryException $e){
            //EN CASO DE UNA EXCEPCION
            Session::flash('error', $e);
            return Redirect::to('distrito');

        }
        

    }
}
