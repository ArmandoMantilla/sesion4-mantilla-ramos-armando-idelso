<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Eliminar Distrito</title>
    @include('layout.script_cabecera')
</head>
<body>
    <div class="container">
        <h3>Eliminar Distrito</h3>
        <div class="row">
            @if(Session('exito'))
            <div class="Aler alert-success">
                {{session('exito')}}
            </div>
            @endif
            @if(Session('error'))
            <div class="Aler alert-danger">
                {{session('error')}}
            </div>
            @endif
        </div>
        <hr>
        <form action="{{route('distrito.destroy',$distrito->id)}}" method="POST">
            @method('DELETE')
            @csrf
            <center>
                <h3>Atencion,Estas a punto de eliminar el siguiente registro:</h3>
                <h4><b>{{$distrito->dis_nombre}} {{$distrito->dis_pais}}</b></h4>
                <hr>
                <button type="submit" class="btn btn-danger btn-sm">Confirmar</button>
                <a href="{{url('distrito')}}" class="btn btn-primary btn-sm">atras</a>
            </center>
        </form>  
    </div>
    @include('layout.script_pie')
</body>
</html>