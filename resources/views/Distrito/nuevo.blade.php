<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo Distrito</title>
    @include('layout.script_cabecera')
</head>
<body>
    <div class="container">
        <h3>Nuevo Distrito</h3>
        <div class="row">
            @if(Session('exito'))
            <div class="Aler alert-success">
                {{session('exito')}}
            </div>
            @endif
            @if(Session('error'))
            <div class="Aler alert-danger">
                {{session('error')}}
            </div>
            @endif
        </div>
        <hr>
        <form action="{{url('distrito')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre (*)</label>
                <input type="text" class="form-control" id="txt_nombre" name="txt_nombre" value="{{old('txt_nombre')}}">
                @error('txt_nombre')
                    <span class="error" role="alert" style="color:red">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">País (*)</label>
                <input type="text" class="form-control" id="txt_pais" name="txt_pais" value="{{old('txt_pais')}}">
            </div>
            @error('txt_pais')
                    <span class="error" role="alert" style="color:red">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            <div class="form-group">
                <label for="exampleInputEmail1">Despartamento (*)</label>
                <input type="text" class="form-control" id="txt_departamento" name="txt_departamento" value="{{old('txt_departamento')}}">
            </div>
            @error('txt_departamento')
                    <span class="error" role="alert" style="color:red">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            <div class="form-group">
                <label for="exampleInputEmail1">Fecha de Fundacion</label>
                <input type="date" class="form-control" id="txt_fundacion" name="txt_fundacion" value="{{old('txt_fundacion')}}">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Población</label>
                <input type="text" class="form-control" id="txt_poblacion" name="txt_poblacion" value="{{old('txt_poblacion')}}">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Ubicacion</label>
                <input type="text" class="form-control" id="txt_ubicacion" name="txt_ubicacion" value="{{old('txt_ubicacion')}}">
            </div>
            <hr>
            <div class="form-group">
                (*) Campos Obligatorios
            </div>  
            <button type="submit" class="btn btn-primary">Registrar</button>
            <a href="{{url('distrito')}}" class="btn btn-primary">Volver</a>
        </form>  
    </div>
    @include('layout.script_pie')
</body>
</html>