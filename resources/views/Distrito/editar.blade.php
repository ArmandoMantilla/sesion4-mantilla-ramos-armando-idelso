<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Distrito</title>
    @include('layout.script_cabecera')
</head>
<body>
    <div class="container">
        <h3>Editar Distrito</h3>
        <div class="row">
            @if(Session('exito'))
            <div class="Aler alert-success">
                {{session('exito')}}
            </div>
            @endif
            @if(Session('error'))
            <div class="Aler alert-danger">
                {{session('error')}}
            </div>
            @endif
        </div>
        <hr>
        <form action="{{route('distrito.update',$distrito->id)}}" method="POST">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre</label>
                <input type="text" class="form-control" id="txt_nombre" name="txt_nombre" required value="{{$distrito->dis_nombre}}">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">País</label>
                <input type="text" class="form-control" id="txt_pais" name="txt_pais" required  value="{{$distrito->dis_pais}}">
            </div>
            </div>
            <div class="container form-group">
                <label for="exampleInputEmail1">Despartamento</label>
                <input type="text" class="form-control" id="txt_departamento" name="txt_departamento" required  value="{{$distrito->dis_departamento}}">
            </div>>
            </div>
            <div class="container form-group">
                <label for="exampleInputEmail1">Fecha de Fundacion</label>
                <input type="date" class="form-control" id="txt_fundacion" name="txt_fundacion" require  value="{{$distrito->dis_fechacreacion}}">
            </div>>
            </div>
            <div class="container form-group">
                <label for="exampleInputEmail1">Población</label>
                <input type="text" class="form-control" id="txt_poblacion" name="txt_poblacion" required  value="{{$distrito->dis_cantidadpoblacion}}">
            </div>>
            </div>
            <div class="container form-group">
                <label for="exampleInputEmail1">Ubicacion</label>
                <input type="text" class="form-control" id="txt_ubicacion" name="txt_ubicacion" required  value="{{$distrito->dis_ubicacion}}">
            </div>>
            </div>
            <button type="submit" class="btn btn-primary">Editar</button>
            <a href="{{url('distrito')}}" class="btn btn-primary">Volver</a>
        </form>  
    </div>
    @include('layout.script_pie')
</body>
</html>