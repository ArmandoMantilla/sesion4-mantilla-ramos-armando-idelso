<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listado de Distritos</title>
    @include('layout.script_cabecera')
</head>
<body>
    <div class="container-fluid">
        <h3>Lista de Distritos:</h3>
        <a class ="btn btn-success btn-sm"href="{{url('distrito/create')}}">Agregar nuevo Distrito</a>
        <div class="">
            <br>
            @if(Session('exito'))
            <div class="Aler alert-success">
                {{session('exito')}}
            </div>
            @endif
            @if(Session('error'))
            <div class="Aler alert-danger">
                {{session('error')}}
            </div>
            @endif
        </div>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">País</th>
                    <th scope="col">Departamento</th>
                    <th scope="col">Fecha de Fundacion</th>
                    <th scope="col">Población</th>
                    <th scope="col">Ubicación</th>
                    <th scope="col">Acciones</th> 
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($registros as $registros)
                <tr>
                    <th scope="row">{{$registros->dis_nombre}}</th>
                    <td>{{$registros->dis_pais}}</td>
                    <td>{{$registros->dis_departamento}}</td>
                    <td>{{$registros->dis_fechacreacion}}</td>
                    <td>{{$registros->dis_cantidadpoblacion}}</td>
                    <td>{{$registros->dis_ubicacion}}</td>
                    <td>
                        <a href="{{route('distrito.edit',$registros->id)}}" class="btn btn-sm btn-info">Editar <i class="fas fa-pencil-alt"></i></a>
                        <a href="{{route('distrito.show',$registros->id)}}" class="btn btn-sm btn-danger">Eliminar <i class="fas fa-trash-alt"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @include('layout.script_pie')
</body>
</html>