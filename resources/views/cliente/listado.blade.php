<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestionar Clientes</title>
</head>
<body>
    
    <h2>Lista de Clientes</h2>
    <a href="{{url('Cliente/create')}}">Nuevo Cliente</a>
    <table>
        <thead>
            <tr>
                <td>Nombre</td>
                <td>Apellido</td>
                <td>DNI</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Armando</td>
                <td>Mantilla</td>
                <td>77702329</td>
            </tr>
        </tbody>
    </table>
</body>
</html>