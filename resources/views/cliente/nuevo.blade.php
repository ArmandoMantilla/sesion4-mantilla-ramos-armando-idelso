<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo Cliente</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <h2>Nuevo Cliente</h2>
        <hr>
        <form>
            <div class="form-group">
                <label for="exampleInputEmail1">Nombres</label>
                <input type="text" class="form-control" id="txt_nombres" name="txt_nombres" required>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Apellidos</label>
                <input type="text" class="form-control" id="txt_apellidos" name="txt_apellidos" required>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">DNI</label>
                <input type="text" class="form-control" id="txt_dni" name="txt_dni" required>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Telefono</label>
                <input type="text" class="form-control" id="txt_telefono" name="txt_telefono">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Correo Electronico</label>
                <input type="email" class="form-control" id="txt_email" name="txt_email">
            </div>
            <button type="submit" class="btn btn-primary">Registrar</button>
        </form>  
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>